
 <?php

 abstract class Forma
 {
    public $tipoDeForma;

    public function imprimeForma()
    {      
    	$this -> calculaArea();
        echo $this-> tipoDeForma . ' com Área de: ' . $this-> calculaArea();
    }

    abstract public function calculaArea();	
  
 }
 
 class Quadrado extends Forma
 {
    public $lado;
    
   public function __construct( float $varLado)
    {
    	$this-> tipoDeForma = "Quadrado";
		$this-> lado = $varLado;
    }
   
     public function calculaArea()
     {
        return $this-> lado * $this-> lado;
     } 	
    
 }

 $obj = new Quadrado(5);
 $obj-> imprimeForma();
 
 echo "\n";
 
 class Retangulo extends Forma
 {
 	
 	public $base;
 	public $altura;
 	
 	public function __construct($base, $altura)
 	{
 		$this-> tipoDeForma = "Retângulo";
 		$this-> base = $base;
 		$this-> altura = $altura;
 	}
 	
 	public function calculaArea()
     {
        return $this-> base * $this-> altura;
     } 	
 } 
 
 $obj1 = new Retangulo(5,10);
 $obj1-> imprimeForma(); 


echo "\n";
 
 class Triangulo extends Forma
 {
 	
 	public $cumprimentoBase;
 	public $altura;
 	
 	public function __construct($cumprimentoBase, $altura)
 	{
 		$this-> tipoDeForma = "Triângulo";
 		$this-> cumprimentoBase = $cumprimentoBase;
 		$this-> altura = $altura;
 	}
 	
 	public function calculaArea()
     {
        return $this-> cumprimentoBase * $this-> altura / 2;
     } 	
 } 
 
 $obj2 = new Triangulo(5,10);
 $obj2-> imprimeForma();
 
 echo "\n";
 
  class Circulo extends Forma
 {
 	
 	public float $raio;
 	
 	public function __construct(float $raio)
 	{
 		$this-> tipoDeForma = "Circulo";
 		$this-> raio = $raio;
 	}
 	
 	public function calculaArea()
     {
        return $this-> raio^2.0 * PI();
     } 	
 } 
 
 $obj3 = new Circulo(10);
 $obj3-> imprimeForma(); 
 
 
 ?>