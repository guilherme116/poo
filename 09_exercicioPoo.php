<?php

abstract class Conta
{

    private $tipoDaConta;
   public function getTipoDaConta(){
       return $this-> tipoDaConta;
   }
        public function setTipoDaConta(string $tipoDaConta){
         $this-> tipoDaConta = $tipoDaConta;
        }
    
    public $Conta;
    public $Saldo;
    public $Agencia;
   /// protected $Saldo;

    public function imprimeConta()
    {
        echo 'CONTA: ' . $this->tipoDaConta . ' Agencia: ' . $this->Agencia . ' Conta:' . $this->Conta . ' Saldo:' . $this->calcularSaldo();
    }



    public function deposito(float $valor)
    {
        if($valor > 0)
        {
            $this->Saldo += $valor;
            echo "Deposito efetuado com sucesso !!!";
        }
        else{
            echo "Valor de depósito invalido";
        }
        //$this-> saldo = $this->saldo + $valor;
        

        
        
    }


    public function saque(float $valor)
    {
        $this->Saldo -= $valor;

        if ($this->saldo >= $valor) {
            $this->saldo -= $valor;
            echo "Saque realizado com sucesso";
        } else {
            echo "Saldo insuficiente!!!<br>";
        }
    }

    abstract public function calcularSaldo();
}

class Especial extends Conta
{
    public $SaldoEspecial;
    public function __construct(string $Agencia, string $Conta, float $SaldoEspecial)
    {
        $this->setTipoDaConta ("Especial") ;
        $this->Agencia = $Agencia;
        $this->Conta = $Conta;
        $this->SaldoEspecial = $SaldoEspecial;
    }
       
    public function calcularSaldo()
    {
        return $this->Saldo + $this->SaldoEspecial;
    }
}

class Poupanca extends Conta
{
    public $Reajuste;

    public function __construct(string $Agencia, string $Conta, float $reajuste)
    {
        $this->setTipoDaConta ("Poupança") ;
        $this->Agencia = $Agencia;
        $this->Conta = $Conta;
        $this->Reajuste = $reajuste;
    }

    public function calcularSaldo()
    {
        return $this->Saldo + ($this->Saldo * $this->Reajuste / 100);
    }
}


$ctaPoupanca = new Poupanca('0002-7', '85588-88', 0.54);

$ctaPoupanca->saldo = -1500;

// não pode acessar atributo protejido
$ctaPoupanca->deposito(1500);

$ctaPoupanca->imprimeConta();

echo "<br>";
$ctaEspecial = new Especial('0055-2', '75588-42', 2300);

$ctaEspecial->deposito(1500);
$ctaEspecial->imprimeConta();
$ctaPoupanca->saque(300);
