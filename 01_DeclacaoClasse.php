<?php


abstract class Forma
{
    public $tipoDeForma = 'Forma Abstrata';

    public function imprimeForma()
    {
        echo $this->tipoDeForma . ' com Área de: ' . $this->calculaArea();
    }
    abstract public function calculaArea();
}


class Quadrado extends Forma
{
    public $lado;

    public function __construct(float $varLado)
    {
        $this->tipoForma = 'Quadarado';
        $this->lado = $varLado;
    }

    public function calculaArea()
    {
        return $this->lado * $this->lado;
    }
}


$obj = new Quadrado(5);
$obj->imprimeForma();

$obj1 = new  Quadrado(100);

$obj1->imprimeForma();

echo "\n";
class Retangulo  extends Forma
{
    public $base;
    public $altura;

    public function __construct( $base, $altura)
    {
        $this-> tipoDeForma = "Retângulo";
        $this->base = $base;
        $this->altura = $altura;
        
    }
    public function calculaArea()
    {
        return $this->base * $this->altura;
    }


}
$obj2 = new  Retangulo(5,10);
$obj2->imprimeForma();

echo "\n";

 class triangulo extends Forma{
     public $cumprimentobase;
     public $altura;
     
     public function __construct($cumprimentobase, $altura)
     {
         $this-> tipoDeForma = "Triângulo";
         $this-> cumprimentobase = $cumprimentobase;
         $this-> altura = $altura;
     }

     public function calculaArea()
     {
         return $this-> cumprimentobase * $this-> altura / 2;

     }


 }
 $obj2 = new triangulo(5,10);
 $obj2-> imprimeForma();


?>