
 <?php
 
header('Content-type: text/html; charset=utf-8');    
setlocale(LC_ALL, 'pt_BR.utf-8');
 
 abstract class Forma
 {
    public $tipoDeForma;

    public function imprimeForma()
    {     
		
    	$this -> calculaArea();
        echo $this-> tipoDeForma . ' com Área de: ' . $this-> calculaArea();
    }

    abstract public function calculaArea();	
  
 }
 
 class Quadrado extends Forma
 {
    public $lado;
    
   public function __construct( float $varLado)
    {
    	$this-> tipoDeForma = "Quadrado";
		$this-> lado = $varLado;
    }
   
     public function calculaArea()
     {
        return $this-> lado * $this-> lado;
     } 	
    
 }

 $obj = new Quadrado(5);
 $obj-> imprimeForma();
 
 echo "\n";
 
 class Retangulo extends Forma
 {
 	
 	public $base;
 	public $altura;
 	
 	public function __construct($base, $altura)
 	{
 		$this-> tipoDeForma = "Retângulo";
 		$this-> base = $base;
 		$this-> altura = $altura;
 	}
 	
 	public function calculaArea()
     {
        return $this-> base * $this-> altura;
     } 	
 } 
 
 $obj1 = new Retangulo(5,10);
 $obj1-> imprimeForma(); 


echo "\n";
 
 class Triangulo extends Forma
 {
 	
 	public $cumprimentoBase;
 	public $altura;
 	
 	public function __construct($cumprimentoBase, $altura)
 	{
 		$this-> tipoDeForma = "Triângulo";
 		$this-> cumprimentoBase = $cumprimentoBase;
 		$this-> altura = $altura;
 	}
 	
 	public function calculaArea()
     {
        return $this-> cumprimentoBase * $this-> altura / 2;
     } 	
 } 
 
 $obj2 = new Triangulo(5,10);
 $obj2-> imprimeForma();
 
 
 ?>