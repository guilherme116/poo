
 <?php

 abstract class Forma
 {
    public $tipoDeForma;

    public function imprimeForma()
    {      
    	$this -> calculaArea();
        echo $this-> tipoDeForma . ' com Àrea de: ' . $this-> calculaArea();
    }

    abstract public function calculaArea();	
  
 }

 class Quadrado extends Forma
 {
    public $lado;
    
   public function __construct( float $varLado)
    {
    	$this-> tipoDeForma = 'Quadrado';
		$this-> lado = $varLado;
    }
   
     public function calculaArea()
     {
        return $this-> lado * $this-> lado;
     } 	
    
 }

 $obj = new Quadrado(5);
 $obj-> imprimeForma(); 
 
 $obj1 = new Quadrado(100);
 $obj1-> imprimeForma();

 ?>