<?php


abstract class Documento
{
  protected  $numero;
  abstract public function eValido();
  abstract public function formata();
  public function setNumero($numero){
    $this-> numero = preg_replace ( '/[^0-9]/', '', $numero);

  }

  public function getNumero(){
      return $this-> numero;
  }
}

class CPF extends Documento{

    public function __construct($numero){
        $this-> setNumero($numero); //expressão regular
    }

    public function eValido(){


    }

    public function formata(){
        //Formato do CPF:###.###.###-##
            return substr($this->numero, 0, 3) . '.' .
            substr($this->numero, 3, 3) . '.' .
            substr($this->numero, 6, 3) . '-'.
           substr($this-> numero, 9 , 2);
    }

}

class CNPJ extends Documento{
  public function __construct($numero){
      $this-> setNumero($numero);
  }
    public function eValido(){


    }

    public function formata(){
  
  {
      
  }
    }


}


class CNH extends Documento{
 private $categoria;
  public function  __construct($numero, $categoria){
    $this-> setNumero($numero);
    $this-> categoria =  $categoria;

  }

 public function eValido(){


}

public function formata(){

}
public function setCategoria($categoria){
    $this->categoria = $categoria;

}

 public function getCategoria(){
     return $this->categoria;
 }
}
//modo de encapsulamento colocar em protect
$cpf = new CPF('115.858.758-88');
echo $cpf-> formata();
/* esta sem proteção*/
$cpf-> setNumero('CPF: 545.855.565-85') ;
echo '</br>';
echo $cpf-> getNumero();

echo '</br>';
$cnpjSenac = new CNPJ('03.709.814/0025-65');
echo $cnpjSenac-> getNumero();
echo '</br>';
$cnhFulano = new CNH('1255656', 'AB');
echo $cnhFulano-> getNumero() . 'cat.:'  .$cnhFulano->getCategoria();

?>